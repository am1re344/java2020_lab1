package FS;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public class FileSystemNode
{
    private String name;
    private String type;
    private Directory parent;

    ReadWriteLock rwlock = new ReentrantReadWriteLock();

    public FileSystemNode(String name, String type, Directory parent)
    {
        this.name = name;
        this.type = type;
        this.parent = parent;
    }

    public FileSystemNode(String name)
    {
        readwrite(() -> this.name = name);
    }

    public String getName()
    {
        return readonly(() -> name);
    }

    public void setName(String name)
    {
        readwrite(() -> this.name = name);
    }

    public String getType()
    {
        return readonly(() -> type);
    }

    public void setType(String type)
    {
        readwrite(() -> this.type = type);
    }

    public Directory getDirectory()
    {
        return parent;
    }

    public void setDirectory(Directory directory)
    {
        readwrite(() -> this.parent = directory);
    }

    public void remove()
    {
        readwrite(() -> {
            if(parent == null) return;
            parent.removeFile(this);
            parent = null;
        });
    }

    public void moveTo(Directory directory)
    {
        readwrite(() -> {
            parent.removeFile(this);
            parent = directory;
            directory.createFile(this);
        });
    }

    protected <T> T readonly(Supplier<T> func)
    {
        rwlock.readLock().lock();
        T ret = func.get();
        rwlock.readLock().unlock();
        return ret;
    }

    protected void readonly(Callback func)
    {
        rwlock.readLock().lock();
        func.call();
        rwlock.readLock().unlock();
    }

    protected <T> T readwrite(Supplier<T> func)
    {
        rwlock.writeLock().lock();
        T ret = func.get();
        rwlock.writeLock().unlock();
        return ret;
    }

    protected void readwrite(Callback func)
    {
        rwlock.writeLock().lock();
        func.call();
        rwlock.writeLock().unlock();
    }

    private interface Callback {
        void call();
    }
}
