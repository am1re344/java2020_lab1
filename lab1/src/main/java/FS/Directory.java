package FS;

import java.util.ArrayList;
import java.util.List;

public class Directory extends FileSystemNode
{
    private static final int DIR_MAX_ELEMS = 15;
    private final ArrayList<? super FileSystemNode> childFiles;

    public Directory(String name, Directory parent)
    {
        super(name, "Directory", parent);
        childFiles = new ArrayList<>();
        if(parent != null) {} //add
    }

    public static Directory create(String name, Directory parent)
    {
        if(name.equals("") || (parent != null && parent.getChildrenCount() == DIR_MAX_ELEMS)) return null;
        return new Directory(name, parent);
    }

    public int getChildrenCount()
    {
        return readonly(() -> childFiles.size());
    }

    public boolean createFile(FileSystemNode fileSystemNode)
    {
        return readwrite(() ->
        {
            if (fileSystemNode != null)
            {
                if (!childFiles.contains(fileSystemNode) && getChildrenCount() < DIR_MAX_ELEMS)
                {
                    childFiles.add(fileSystemNode);
                    return true;
                }
            }
            return false;
        });
    }

    public void removeFile(FileSystemNode fileSystemNode)
    {
        readwrite(() -> childFiles.remove(fileSystemNode));
    }

    public ArrayList<? super FileSystemNode> getChildFiles()
    {
        return readonly(() ->  (ArrayList<? super FileSystemNode>)childFiles.clone());
    }

    public static int getDirMaxElms()
    {
        return DIR_MAX_ELEMS;
    }
}
