package FS;

public class BinaryFile extends FileSystemNode
{
    private final String content;

    public BinaryFile(String name, Directory parent, String content)
    {
        super(name, "Binary file", parent);
        this.content = content;
        parent.createFile(this);
    }

    public BinaryFile(String name, String content)
    {
        super(name);
        this.content = content;
    }

    public static BinaryFile create(String name, Directory parent, String content)
    {
        if(name.equals("") || (parent != null && parent.getChildrenCount() < Directory.getDirMaxElms())) return null;
        return new BinaryFile(name, parent, content);
    }

    public String read()
    {
        return readonly(() -> content);
    }
}
