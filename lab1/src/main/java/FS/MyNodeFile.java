package FS;

public class MyNodeFile extends FileSystemNode
{
    private String content;

    public MyNodeFile(String name, Directory parent, String content)
    {
        super(name, "My Node File", parent);
        this.content = content;
    }

    public static MyNodeFile create(String name, Directory parent, String content)
    {
        if(name.equals("") || (parent != null && parent.getChildrenCount() < Directory.getDirMaxElms())) return null;
        return new MyNodeFile(name, parent, content);
    }

    public String read()
    {
        return readonly(() -> content);
    }

    public void rewrite(String content)
    {
        readwrite(() -> this.content = content);
    }

    public void append(String additionalContent)
    {
        readwrite(() -> this.content += additionalContent);
    }
}
